package indexer

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/u869/estruturadados2/src/structures"
)

func FrequencyN(args []string) {

	root := structures.NewTrie()

	FileReader(root, args[2])
	maxNumberToShow, err := strconv.Atoi(args[1])
	if err != nil {
		panic("Não foi possivel converter N para inteiro")
	}
	//root.IterateTrieWords()
	structures.Sorted(maxNumberToShow)

}

func CountWords(filePath string, trie *structures.TrieNode) {
	file, err := os.Open(filePath)
	if err != nil {
		panic("Não foi possivel ler o arquivo")
	}
	if err != nil {
		panic("Não foi possivel ler N")
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	for scanner.Scan() {
		trie.Insert(scanner.Text())
	}
}

func FreqWord(args []string) {
	searchTerm := strings.ToLower(args[1])
	root := structures.NewTrie()
	FileReader(root, args[2])
	word := root.SearchWord(searchTerm)
	if word == nil {
		fmt.Println("Não foi encontrado o termo")
		return
	}
	fmt.Println(word.CurrentWord, word.Count)
}

func SearchTerm(args []string) {
	searchTermArg := strings.ToLower(args[1])
	searchTerms := strings.Split(strings.ReplaceAll(searchTermArg, "\"", ""), " ")
	files := args[2:]
	roots := make([]*structures.TrieNode, len(files))
	relevances := make([]RelevanceData, len(files))

	for i, file := range files {
		roots[i] = structures.NewTrie()

		//CountWords(file, roots[i])
		FileReader(roots[i], file)
		for _, searchTerm := range searchTerms {
			term := roots[i].SearchWord(searchTerm)
			if relevances[i].relevance == 0.0 {
				relevances[i] = RelevanceData{
					filePath:  file,
					relevance: calculateRelevance(searchTerm, term.Count, roots[i].TotalWords),
				}
			} else {
				relevances[i].relevance += calculateRelevance(searchTerm, term.Count, roots[i].TotalWords)
			}

		}

	}
	sort.Slice(relevances, func(i, j int) bool {
		return relevances[i].relevance > relevances[j].relevance
	})
	for i := 0; i < len(relevances); i++ {
		fmt.Println(relevances[i])
	}
}

func calculateRelevance(term string, count int, max int) float64 {
	return (float64(count) / float64(max)) * math.Log10((float64(max) / float64(count)))
}

type RelevanceData struct {
	filePath  string
	relevance float64
}
