# Trabalho Indexer

## Alunos: 
 - Christian Dueck
 - Jhionan Lara dos Santos
 - Leonardo Castro De Oliveira
---

### Binário executavel:
Os binários executáveis estão disponível no projeto na pasta `gitlab.com/binario`



### Instalação:


Projeto feito em Go, para instalar compilador siga as instruções:    
1. Download última versão de [GO aqui](https://go.dev/dl/)

Linux:

2. Step 2: Unzip o tar.gz e mova para `/usr/local` directory

    `tar -C /usr/local -xzf go1.15.6.linux-amd64.tar.gz`
3. Adicione Go nas variáveis de ambiente,

    abra o arquivo `.bashrc` ou `.bash_profile`
    
    `vim ~/.bash_profile`
    e adicione: `export PATH=$PATH:/usr/local/go/bin`
4. Reinicie o terminal e teste:
`go version`
___
### Compilação:
1. clone o repositório
2. na raiz do repositório digite: `go build .`
3. execute: `./estruturadados2 [args]`


#### Argumentos: 
`--freq N ARQUIVO`
    Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em
    ordem decrescente de ocorrência.

`--freq-word PALAVRA ARQUIVO`
    Exibe o número de ocorrências de PALAVRA em ARQUIVO. 

`--search TERMO ARQUIVO [ARQUIVO ...]`
    Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por 
    TERMO. A listagem é apresentada em ordem descrescente de relevância. 
    TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre 
    àspas.

Mais detalhes [aqui](https://gitlab.com/ds143-alexkutzke/material/-/blob/main/trabalho_2021_02.md)