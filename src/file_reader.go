package indexer

import (
	"bufio"
	"os"
	"runtime"
	"strings"
	"sync"

	"gitlab.com/u869/estruturadados2/src/structures"
)

const mb = 1024 * 1024

// const gb = 1024 * mb

func FileReader(trie *structures.TrieNode, file string) {

	wg := new(sync.WaitGroup)

	channel := make(chan (string))

	// Done is a channel to signal the end of file reading.
	done := make(chan (bool), 1)

	go func() {
		for s := range channel {
			trie.Insert(s)
		}
		done <- true
	}()

	//current bytes of file offset
	var current int64 = 0
	var limit int64 = 500 * mb

	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)

		go func(offset int64) {
			read(offset, file, channel, wg)

		}(current)

		// Increment the current by 1+(last byte read by previous thread).
		current += limit + 1
	}

	// Wait for all go routines to complete.
	wg.Wait()

	close(channel)
	<-done
	close(done)

}

func read(offset int64, fileName string, channel chan (string), wg *sync.WaitGroup) {

	file, err := os.Open(fileName)

	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Move the pointer of the file to the start of designated chunk.
	file.Seek(offset, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), " ")
		for _, value := range s {
			if value != "" {
				channel <- value
			}
		}
	}
	defer wg.Done()
}
