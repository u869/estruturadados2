package main

import (
	"fmt"
	"os"

	indexer "gitlab.com/u869/estruturadados2/src"
)

func main() {

	args := os.Args[1:]
	fmt.Println(args)

	if len(args) < 2 {
		panic("Falta argumentos para rodar")
	}

	switch args[0] {
	case "--freq":
		indexer.FrequencyN(args)
	case "--freq-word":
		indexer.FreqWord(args)
	case "--search":
		indexer.SearchTerm(args)
	default:
		panic("Argumento inválido")
	}

	fmt.Println("Programa finalizado")
}
