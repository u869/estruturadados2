package structures

import (
	"fmt"
	"regexp"
	"strings"
)

var frequencyList = make(map[int]map[string]bool)

type TrieNode struct {
	//Letter
	Char string
	//Helper to count faster
	CurrentWord string
	//Possible children
	Children [26]*TrieNode
	//Count world ended here
	Count int
	//only root have this data
	TotalWords int
}

func newNode(char string, currentWord string) *TrieNode {
	node := &TrieNode{Char: char, CurrentWord: currentWord + char}
	for i := 0; i < 26; i++ {
		node.Children[i] = nil
	}
	return node
}

func NewTrie() *TrieNode {
	return newNode("", "")
}

func (t *TrieNode) Insert(word string) error {

	strippedWord := stripWord(word)
	if len(strippedWord) <= 2 {
		return nil
	}
	current := t

	if current.Char == "" {
		current.TotalWords++
	}

	for i := 0; i < len(strippedWord); i++ {

		index := strippedWord[i] - 'a'
		if current.Children[index] == nil {
			current.Children[index] = newNode(string(strippedWord[i]), current.CurrentWord)
		}
		current = current.Children[index]
		//since we want to support autocomplete
	}
	current.Count++
	addToFrequencyList(current.Count, current.CurrentWord)
	return nil
}

func addToFrequencyList(count int, word string) {
	if frequencyList[count] == nil {
		frequencyList[count] = make(map[string]bool)
	}
	frequencyList[count][word] = true
	delete(frequencyList[count-1], word)
}

func stripWord(word string) string {
	reg, err := regexp.Compile("[^a-zA-Z]+")
	if err != nil {
		panic(err)
	}
	strippedWord := strings.ToLower(reg.ReplaceAllString(word, ""))
	return strippedWord
}

func (t *TrieNode) SearchWord(word string) *TrieNode {
	strippedWord := stripWord(word)
	current := t
	for i := 0; i < len(strippedWord); i++ {
		index := strippedWord[i] - 'a'
		//if the current node is nil, then the word is not in the trie
		if current == nil || current.Children[index] == nil {
			return nil
		}
		current = current.Children[index]
	}
	return current
}

func (t *TrieNode) IterateTrieWords() {
	current := t

	if current.Count > 0 {
		fmt.Println(current.CurrentWord, current.Count)
	}

	for i := 0; i < 26; i++ {
		if current.Children[i] != nil {
			current.Children[i].IterateTrieWords()
		}

	}
}

func Sorted(max int) {
	if max > len(frequencyList) {
		max = len(frequencyList)
	}
	for i, j := len(frequencyList), max; i >= 0 && j > 0; i-- {

		if len(frequencyList[i]) > 0 {
			j--
			for key := range frequencyList[i] {
				fmt.Println(key, i)
			}
		}
	}
}
